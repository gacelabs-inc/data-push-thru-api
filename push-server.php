<?php
date_default_timezone_set('Asia/Manila');
require __DIR__ . '/vendor/autoload.php';

$loop = React\EventLoop\Factory::create();
$pusher = new WebApp\Pusher;

/*Listen for the web server to make a ZeroMQ push after an ajax request*/
$context = new React\ZMQ\Context($loop);
$pull = $context->getSocket(ZMQ::SOCKET_PULL);
$pull->bind('tcp://127.0.0.1:5555'); /*Binding to 127.0.0.1 means the only client that can connect is itself*/
$pull->on('message', array($pusher, 'onDataEntry'));

$wsserver = new Ratchet\WebSocket\WsServer(new Ratchet\Wamp\WampServer($pusher));
$realtimeApp = new Ratchet\Http\OriginCheck($wsserver);
$realtimeApp->allowedOrigins[] = 'local.datapushthru.com.ph'; /*own server is definitely allowed*/
$realtimeApp->allowedOrigins[] = 'datapushthru.com'; /*own server is definitely allowed*/

$DB = $pusher->get_DB();
$companies = $DB->query("SELECT * FROM companies WHERE allowed = 1");
if ($companies->num_rows) {
	$companies = $companies->fetchAll();
	foreach ($companies as $key => $row) {
		$realtimeApp->allowedOrigins[] = $row->domain;
	}
}

/*Set up our WebSocket server for clients wanting real-time updates*/
$webSock = new React\Socket\Server('0.0.0.0:8080', $loop); /*Binding to 0.0.0.0 means remotes can connect*/
$webServer = new Ratchet\Server\IoServer(
	new Ratchet\Http\HttpServer($realtimeApp),
	$webSock
);

echo "\n\n".date('F j, Y | g:i:s a').": Allowed domains => ".json_encode($realtimeApp->allowedOrigins, JSON_PRETTY_PRINT);
echo "\n\n".date('F j, Y | g:i:s a').": Listening and waiting requests";
$loop->run();