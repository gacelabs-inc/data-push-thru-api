<?php

$context = new ZMQContext();
$broadcast = $context->getSocket(ZMQ::SOCKET_PUB);
$broadcast->bind("tcp://*:5566", true);

$pull_listener = $context->getSocket(ZMQ::SOCKET_PULL);
$pull_listener->bind("tcp://*:5567", true);

echo "Listening...\n";
while (true) {
	$data = $pull_listener->recv();
	echo "Data: ".$data;
	$broadcast->send($data);
	sleep(1);
}