<?php

$channel = htmlspecialchars($_POST['channel']);
$event = htmlspecialchars($_POST['event']);

$context = new ZMQContext();
$pusher = $context->getSocket(ZMQ::SOCKET_PUSH);
$pusher->connect("tcp://127.0.0.1:5567", true);

// $pusher->send($event."\n", ZMQ::MODE_SNDMORE);
if ($event == 'join') {
	$pusher->send($channel."\n");
} elseif ($event == 'clear') {
	$pusher->send('clear'."\n");
} else {
	echo 'event sent!';
	$pusher->send($channel.': '.$event."\n");
}