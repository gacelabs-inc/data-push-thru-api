<?php

$context = new ZMQContext();
$requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);
$requester->connect("tcp://127.0.0.1:5555");

$puller = new ZMQSocket($context, ZMQ::SOCKET_PULL);
$puller->connect("tcp://127.0.0.1:5554");

for ($request_nbr = 0; $request_nbr != 10; $request_nbr++) {
	// printf ("Sending request %d...\n", $request_nbr);
	$requester->send("Hello");
	$reply = $requester->recv();
	printf ("Received reply %d: [%s]\n", $request_nbr, $reply);

	$msg = $puller->recv();
	printf ("Send %s to others", $msg);
}