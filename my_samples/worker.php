<?php

if ($_POST) {
	$context = new ZMQContext();
	$listener = new ZMQPoll();
	
	$subs = $context->getSocket(ZMQ::SOCKET_SUB);
	$subs->connect("tcp://127.0.0.1:5566");

	$pusher = $context->getSocket(ZMQ::SOCKET_PUSH);
	$pusher->connect("tcp://127.0.0.1:5567");

	if ($_POST['event'] == 'subscribe') {
		$subs->setSockOpt(ZMQ::SOCKOPT_SUBSCRIBE, $_POST['channel']);		
		$listener->add($subs, ZMQ::POLL_IN);
	} elseif ($_POST['event'] == 'message') {
		$listener->add($pusher, ZMQ::POLL_IN);
	}

	$read = $write = array();
	while (true) {
		$event = $listener->poll($read, $write, 1000000);
		if ($event > 0) {
			foreach ($read as $socket) {
				$evt = $socket->recv();
				echo $evt;
				exit();
	        }
		}
		ob_flush();
		flush();
	}
}