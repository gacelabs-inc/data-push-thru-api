<?php echo "<pre>"; print_r("&#60;&#63;php
/**
 * GaceLabs, Inc.
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2016 - 2018.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	GaceLabs
 * @author	GaceLabs Dev Team
 * @copyright	Copyright (c) 2016 - 2018, GaceLabs, Inc. (https://gacelabs.com/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://datapushthru.com
 * @since	Version 1.0.0
 * @filesource
 */

/**
 * GaceLabs PushThru Class
 *
 * This class enables the creation of calendars
 *
 * @package	GaceLabs
 * @subpackage	Libraries
 * @category	Libraries
 * @author 	GaceLabs Dev Team
 * @link 	https://datapushthru.com/guide/documentation
 */
class PushThru {

	/**
	 * Data PushThru application URL
	 *
	 * @var string
	 */
	public &#36;app_url = '".$base_url."';

	/**
	 * Whether the application is loaded or not
	 *
	 * @var bool
	 */
	public &#36;is_loaded = FALSE;

	/**
	 * Client or Account application key
	 *
	 * @var string
	 */
	protected &#36;app_key = NULL;

	/**
	 * Client / Account application secret key
	 *
	 * @var string
	 */
	protected &#36;app_secret = NULL;

	/**
	 * Header response code
	 *
	 * @var string
	 */
	public &#36;response_code = 403;

	/**
	 * Header response code message
	 *
	 * @var string
	 */
	public &#36;response_text = 'Connection did not pass with your App key preference.';

	// --------------------------------------------------------------------

	/**
	 * Class constructor
	 *
	 * Loads the PushThru application using the app_key.
	 *
	 * @param	array	&#36;config	allowed index is app_key only 
	 * @return	void
	 */
	public function __construct(&#36;config='')
	{
		if (is_array(&#36;config) OR is_object(&#36;config)) {
			if (is_object(&#36;config)) &#36;config = (array) &#36;config;
			if (isset(&#36;config['app_key'])) {
				&#36;this->initialize(&#36;config['app_key']);
			} else {
				throw new Exception('Index app_key not found', 403);
			}
		} else {
			if (&#36;config != '') {
				&#36;app_key = &#36;config;
				&#36;this->initialize(&#36;app_key);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Initialize the constructor
	 *
	 * @param	string	the client's application key
	 * @return	PushThru Object Class
	 */
	public function initialize(&#36;app_key=NULL)
	{
		&#36;this->clear();
		&#36;this->app_key = &#36;app_key;
		&#36;this->is_loaded = &#36;this->load_app();
		return &#36;this;
	}

	// ---------------------------------- Private methods ----------------------------------

	/**
	 * Checks and loads the PushThru application 
	 *
	 * @return	Boolean if the client is registered with the app_key given
	 */
	private function load_app()
	{
		&#36;response = &#36;this->do_curl('api/script/'.&#36;this->app_key.'/2');
		&#36;data = json_decode(&#36;response, TRUE);
		if (is_array(&#36;data) AND count(&#36;data)) {
			&#36;this->app_secret = &#36;data['app_secret'];
			&#36;this->catch_response('successful');
			return TRUE;
		} else {
			&#36;this->catch_response('forbidden');
		}
		return FALSE;
	}

	// --------------------------------------------------------------------

	/**
	 * Performs a request to the datapushthru server and catch the response
	 *
	 * @param	string	uri segment or the function call
	 * @param	array	request params for the function call
	 * @return	mixed 	servers response
	 */
	private function do_curl(&#36;segment='', &#36;data=array())
	{
		&#36;ch = curl_init();
		curl_setopt(&#36;ch, CURLOPT_URL, &#36;this->app_url.&#36;segment);
		curl_setopt(&#36;ch, CURLOPT_POST, 1);
		curl_setopt(&#36;ch, CURLOPT_POSTFIELDS, http_build_query(&#36;data));
		// receive server response...
		curl_setopt(&#36;ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt(&#36;ch, CURLOPT_FOLLOWLOCATION, TRUE);
		&#36;server_output = curl_exec(&#36;ch);
		
		curl_close(&#36;ch);

		// echo &#36;server_output; exit();
		return &#36;server_output;
	}

	// --------------------------------------------------------------------

	/**
	 * Catch the server's response
	 *
	 * @param	string	response term / type
	 * @return	mixed 	PushThru Object Class
	 */
	private function catch_response(&#36;response='forbidden')
	{
		switch (&#36;response) {
			case 'non_existed_channel':
				&#36;this->response_code = 403;
				&#36;this->response_text = 'Cant create event with non existing channel.';
				break;
			case 'empty_params':
				&#36;this->response_code = 404;
				&#36;this->response_text = 'Expected arguments are 3, empty given.';
				break;
			case 'channel_removed':
				&#36;this->response_code = 404;
				&#36;this->response_text = 'Channel has been removed.';
				break;
			case 'channel_created':
				&#36;this->response_code = 200;
				&#36;this->response_text = 'Channel has been created.';
				break;
			case 'transmitted':
				&#36;this->response_code = 200;
				&#36;this->response_text = 'Data has been transmitted to channel.';
				break;
			case 'successful':
				&#36;this->response_code = 200;
				&#36;this->response_text = 'Connection Successful.';
				break;
			
			default: /*forbidden*/
				&#36;this->response_code = 403;
				&#36;this->response_text = 'Connection did not pass with your App key preference.';
				break;
		}
		return &#36;this;
	}

	// ---------------------------------- End of private methods ----------------------------------

	/**
	 * Create and trigger the event to the specific channels
	 *
	 * @param	string	event name
	 * @param	string	channel name
	 * @param	array	data to transmit
	 * @return	mixed 	PushThru Object Class
	 */
	public function trigger(&#36;event='', &#36;channel='', &#36;push_data=array())
	{
		if (&#36;this->is_loaded) {
			if (&#36;channel != '' AND &#36;event != '') {
				&#36;check = &#36;this->do_curl('app/channels/'.&#36;this->app_secret.'/0', array('channel' => &#36;channel));
				if (&#36;check) {
					&#36;data_object = array(
						'event' => &#36;event,
						'app_key' => &#36;this->app_key,
						'exclude' => 0,
						'eligible' => 0,
						'data' => &#36;push_data
					);
					&#36;response = &#36;this->do_curl('app/transmit/'.&#36;this->app_secret.'/2', &#36;data_object);
					&#36;type = 'forbidden';
					if (count(&#36;response = json_decode(&#36;response, TRUE))) {
						if (&#36;response['code'] == 200) {
							&#36;type = 'transmitted';
						} else {
							&#36;type = 'non_existed_channel';
						}
					}
					&#36;this->catch_response(&#36;type);
				} else {
					&#36;this->catch_response('non_existed_channel');
				}
			} else {
				&#36;this->catch_response('empty_params');
			}
		}
		return &#36;this->catch_response('forbidden');
	}

	// --------------------------------------------------------------------

	/**
	 * Clears all properties and set the default values
	 *
	 * @param	boolean	whether initialize the app or referencial
	 * @return	mixed 	PushThru Object Class
	 */
	public function clear(&#36;initialize=FALSE)
	{
		&#36;this->is_loaded = FALSE;
		&#36;this->app_secret = NULL;
		&#36;this->response_code = 403;
		&#36;this->response_text = 'Connection did not pass with your App key preference.';
		if (&#36;initialize) &#36;this->load_app();
		return &#36;this;
	}
}"); echo "</pre>";?>
