/*!
 * <?php echo APP_NAME;?> JavaScript Library v1.0.0 * http://www.datapushthru.com/
 *
 * Copyright 2018, Data<?php echo APP_NAME;?>.com
 */
 (function webpackUniversalModuleDefinition(root, factory) {
 	if(typeof exports === "object" && typeof module === "object") {
 		module.exports = factory();
 	} else if(typeof define === "function" && define.amd) {
 		define([], factory);
 	} else if(typeof exports === "object") {
 		exports["<?php echo APP_NAME;?>"] = factory();
 	} else {
 		root["<?php echo APP_NAME;?>"] = factory();
 	}
 })(this, function() {
 	return (function(modules) { 
 		var installedModules = {};

 		function __webpack_require__(moduleId) {
 			if(installedModules[moduleId]) {
 				return installedModules[moduleId].exports;
 			}

 			var module = installedModules[moduleId] = {
 				exports: {},
 				id: moduleId,
 				loaded: false
 			};

 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
 			module.loaded = true;
 			return module.exports;
 		}

 		__webpack_require__.m = modules;
 		__webpack_require__.c = installedModules;
 		__webpack_require__.p = "";
 		return __webpack_require__(0);
 	})
 	/************************************************************************/
 	([
 		/* 0 */
 		(function(module, exports, __webpack_require__) {
 			"use strict";
 			var pushthru = __webpack_require__(1);
 			module.exports = pushthru["default"];
 		}),
 		/* 1 */
 		(function(module, exports, __webpack_require__) {
 			"use strict";
 			var runtime = __webpack_require__(2);
 			var factory = __webpack_require__(3);
 			var <?php echo APP_NAME;?> = (function () {
 				function <?php echo APP_NAME;?>(app_key) {
 					var _this = this;
 					checkAppKey(app_key);

 					<?php echo APP_NAME;?>.key = app_key;
 					this.events = factory["default"].createEvents();
 					this.stashes = factory["default"].createStashes();
 					this.handshakes = factory["default"].createHandshakes();

 					if (<?php echo APP_NAME;?>.is_loaded) {
	 					var i = setInterval(function() {
		 					if (document.readyState === "complete") {
			 					clearInterval(i);
		 						_this.handshakes.startAuth(app_key, function() {
		 							_this.handshakes.connect(function() {
		 								_this.stashes.exec(function(stashes) {
		 									/*stashes.clear();*/ 
		 								});
		 							}, _this.stashes);
		 						});
		 					}
	 					}, 1000);
 					}
 				}
 				<?php echo APP_NAME;?>.run = function () {
 					<?php echo APP_NAME;?>.is_loaded = true;
 				};
 				<?php echo APP_NAME;?>.prototype.log = function (message, type, force) {
 					runtime["default"].log(message, type, force);
 				};
 				/*manage channels*/
 				<?php echo APP_NAME;?>.prototype.allChannels = function () {
 					return this.events.channels.all();
 				};
 				<?php echo APP_NAME;?>.prototype.channel = function (name) {
 					return this.events.channels.find(name);
 				};
 				<?php echo APP_NAME;?>.prototype.subscribe = function (name) {
 					if (this.handshakes.connected) {
 						this.events.channels.add(name);
 					} else {
 						this.stashes.add(name, this.events.channels, [name], 'add');
 					}
					return this;
 				};
 				<?php echo APP_NAME;?>.prototype.unsubscribe = function (name) {
 					return this.events.channels.remove(name);
 				};
 				/*manage events*/
 				<?php echo APP_NAME;?>.prototype.allEvents = function () {
 					return this.events.all();
 				};
 				<?php echo APP_NAME;?>.prototype.event = function (name) {
 					return this.events.find(name);
 				};
 				<?php echo APP_NAME;?>.prototype.bind = function (name, channel, options) {
 					if (this.handshakes.connected) {
 						this.events.add(name, channel, options);
 					} else {
 						this.stashes.add(name, this.events, [name, channel, options], 'add');
 					}
					return this;
 				};
 				<?php echo APP_NAME;?>.prototype.listen = function (name, options) {
 					if (this.handshakes.connected) {
 						this.events.bind(name, options);
 					} else {
 						this.stashes.add(name, this.events, [name, options], 'bind');
 					}
					return this;
 				};
 				<?php echo APP_NAME;?>.prototype.unbind = function (name) {
 					return this.events.remove(name);
 				};
 				<?php echo APP_NAME;?>.prototype.trigger = function (event, channel, data) {
 					if (this.handshakes.connected) {
 						this.events.send(event, channel, data);
 					} else {
 						this.stashes.add(event, this.events, [event, channel, data], 'send');
 					}
					return this;
 				};

 				<?php echo APP_NAME;?>.is_loaded = false;
 				<?php echo APP_NAME;?>.version = '<?php echo APP_VERSION;?>';
 				<?php echo APP_NAME;?>.key = null;
 				<?php echo APP_NAME;?>.Runtime = runtime["default"];
 				return <?php echo APP_NAME;?>;
 			}());
 			exports.__esModule = true;
 			exports["default"] = <?php echo APP_NAME;?>;
 			function checkAppKey(key) {
 				if (key === null || key === undefined) {
 					throw "You must pass your app key when you instantiate <?php echo APP_NAME;?>.";
 				}
 			}
 			runtime["default"].setup(<?php echo APP_NAME;?>);
 		}),
 		/* 2 */
 		(function(module, exports) {
 			"use strict";
 			var Runtime = {
 				ws_host: '<?php echo $_SERVER['HTTP_HOST'];?>',
 				ws_port: <?php echo WS_PORT;?>,
 				wss_port: <?php echo WSS_PORT;?>,
 				api_url: '<?php echo base_url();?>',
 				endpoint: '<?php echo $endpoint;?>',
 				setup: function (PushThruClass) {
 					var _this = this;
 					window.PushThru = PushThruClass;
 					var initializeOnDocumentBody = function () {
 						_this.onDocumentBody(PushThruClass.run);
 					};
 					checkScript((typeof window.jQuery == 'function' || typeof window.$ == 'function'), _this.api_url+'assets/public/js/jquery.min.js', true).then(() => {
 						checkScript((typeof window.JSON == 'object'), _this.api_url+'assets/public/js/json2.js', false);
 					}).then(() => {
 						checkScript(typeof window.ab == 'object', _this.api_url+'assets/public/js/autobahn.js', false);
 					}).then(() => {
 						checkScript((typeof window.jQuery.ajaxq == 'function' || typeof window.$.ajaxq == 'function'), _this.api_url+'assets/public/js/ajaxq.js', false);
 					}).then(() => {
 						return initializeOnDocumentBody();
 					});
 				},
 				getDocument: function () {
 					return document;
 				},
 				getProtocol: function () {
 					return this.getDocument().location.protocol;
 				},
 				onDocumentBody: function (callback) {
 					var _this = this;
 					if (document.body) {
 						callback();
 					} else {
 						setTimeout(function () {
 							_this.onDocumentBody(callback);
 						}, 0);
 					}
 				},
 				log: function(data, type, force) {
 					switch (type) {
 						case 'warn':
 						console.warn("%c\u2620 "+data, "font-weight:bold;line-height:18px;");
 						break;
 						case 'error':
 						console.error("%c\u26d4 "+data, "font-weight:bold;line-height:18px;");
 						break;
 						default:
 						console.log("%c\u26a1 "+data, "font-weight:bold;line-height:18px;");
 						break;
 					}
 				}
 			};
 			exports.__esModule = true;
 			exports["default"] = Runtime;
 			function loadScript(url, is_in_head) {
				return new Promise(function(resolve, reject) {
 					var script = document.createElement("script");
					script.type = 'text/javascript';
 					script.onload = resolve;
 					script.onerror = reject;
 					script.src = url;
 					if (is_in_head) {
 						document.getElementsByTagName("head")[0].appendChild(script);
 					} else {
 						$(script).insertBefore($('#push-thru-scripts'));
 					}
 				});
 			}
 			function checkScript(isSet, url, is_in_head) {
 				if (isSet) {
					return Promise.resolve();
				} else {
					return loadScript(url, is_in_head);
				}
 			}
 		}),
 		/* 3 */
 		(function(module, exports, __webpack_require__) {
 			"use strict";
 			var events = __webpack_require__(4);
 			var channels = __webpack_require__(5);
 			var handshakes = __webpack_require__(6);
 			var stashes = __webpack_require__(7);
 			var Factory = {
 				createChannels: function () {
 					return new channels["default"]();
 				},
 				createEvents: function () {
 					return new events["default"]();
 				},
 				createStashes: function () {
 					return new stashes["default"]();
 				},
 				createHandshakes: function () {
 					return new handshakes["default"]();
 				}
 			};
 			exports.__esModule = true;
 			exports["default"] = Factory;
 		}),
 		/* 4 */
 		(function(module, exports, __webpack_require__) {
 			"use strict";
 			var pushthru = __webpack_require__(1);
 			var runtime = __webpack_require__(2);
 			var factory = __webpack_require__(3);
 			var Events = (function () {
 				function Events() {
 					this.list = {};
 					this.channels = factory["default"].createChannels();
 				}
 				Events.prototype.add = function (event, channel, options) {
					if (this.channels.find(event+':'+channel) == undefined) {
						this.channels.add(event+':'+channel);
						if (typeof options == 'function') {
							options = {callback: options};
						} else if (Array.isArray(options) || typeof options == 'number') {
							options = {
								callback: function() {
									return options;
								}
							};
						}
						this.channels.merge(event+':'+channel, options);
					}
					var eventFunction = function(eventname, obj) {
	 					if (typeof options.callback == "function") {
	 						options.callback(obj, eventname, channel);
	 					} else {
	 						runtime["default"].log("Event Callback is not a function on binded event "+event+" for channel "+channel, "error");
	 					}
	 				};
	 				pushthru["default"].socket.subscribe(event, eventFunction);
	 				this.list[event] = pushthru["default"].socket;
	 				return this;
 				};
 				Events.prototype.bind = function (event, options) {
					this.channels.remove(this.channels.saved);
					this.add(event, this.channels.saved, options);
					this.channels.saved = null;
 					return this;
 				};
 				Events.prototype.all = function () {
 					return this.list;
 				};
 				Events.prototype.find = function (event) {
 					return this.list[event];
 				};
 				Events.prototype.remove = function (event, callback) {
 					var socket = this.list[event];
 					delete this.list[event];
 					socket.unsubscribe(event, callback);
	 				return this;
 				};
 				Events.prototype.send = function (event, channel, push_data) {
	 				if (typeof this.channels.find(event+':'+channel) != 'undefined' && typeof this.find(event) != 'undefined') {
	 					var event_options = this.channels.find(event+':'+channel);
	 					/*var socket = this.find(event);*/
	 					var data_obj = {
	 						'event': event,
	 						'app_key': pushthru["default"].key,
	 						'exclude': 0,
	 						'eligible': 0,
	 						'data': (typeof push_data != 'undefined' ? push_data : {})
	 					}

	 					if (typeof event_options.blackListed == 'boolean' && event_options.blackListed) {
	 						data_obj.exclude = 1;
	 					}
	 					if (typeof event_options.whiteListed == 'boolean' && event_options.whiteListed) {
	 						data_obj.eligible = 0;
	 					}

	 					var settings = {
	 						url: runtime["default"].endpoint,
	 						type: 'post',
	 						data: data_obj,
	 						success: function(response) {
	 							if (typeof event_options.ajax_settings != 'undefined') {
	 								$.ajaxq('owners_send', event_options.ajax_settings);
	 							}
	 							runtime["default"].log("Data transmitted!");
	 						},
	 						error: function(xhr, code, status) {
	 							if (xhr.status == 404) {
	 								runtime["default"].log("You are not allowed transmitting data!", "error");
	 							} else {
	 								runtime["default"].log("Your APP key might be expired or did not pass, Please settle this @ "+runtime["default"].api_url+"support", "error");
	 							}
	 						}
	 					}
	 					$.ajaxq('default_send', settings);
	 				} else {

	 				}
	 				return this;
 				};
 				return Events;
 			}());
 			exports.__esModule = true;
 			exports["default"] = Events;
 		}),
 		/* 5 */
 		(function(module, exports, __webpack_require__) {
 			"use strict";
 			function _extend(a, b) { for(var key in b) { if(b.hasOwnProperty(key)) {a[key] = b[key];} return a; } }
 			var pushthru = __webpack_require__(1);
 			var runtime = __webpack_require__(2);
 			var factory = __webpack_require__(3);
 			var Channels = (function () {
 				function Channels() {
 					this.list = {};
 					this.saved = null;
 				}
 				Channels.prototype.add = function (name) {
 					if (this.find(name) == undefined) {
 						this.list[name] = {whiteListed: false, blackListed: false};
 						this.saved = name;
 						submitChannel(name, 'add_channel');
 					}
 					return this;
 				};
 				Channels.prototype.all = function () {
 					return this.list;
 				};
 				Channels.prototype.find = function (name) {
 					return this.list[name];
 				};
 				Channels.prototype.remove = function (name) {
 					delete this.list[name];
					submitChannel(name, 'remove_channel');
 					return this;
 				};
 				Channels.prototype.merge = function (name, data) {
 					_extend(this.list[name], data);
 					return this.list;
 				};
 				return Channels;
 			}());
 			exports.__esModule = true;
 			exports["default"] = Channels;
 			function submitChannel(name, type) {
				var settings = {
					url: runtime["default"].endpoint,
					type: 'post',
					data: {'type': type, 'app_key': pushthru["default"].key, 'channel': name},
					crossDomain: true,
					success: function(response) {
						if (type == 'add_channel') {
							runtime["default"].log(name+" channel subscribed!");
						} else {
							runtime["default"].log(name+" channel unsubscribed!");
						}
					},
					error: function(xhr, code, status) {
						if (xhr.status == 404) {
							runtime["default"].log("You are not allowed transmitting data!", "error");
						} else {
							runtime["default"].log("Your APP key might be expired or did not pass, Please settle this @ "+runtime["default"].api_url+"support", "error");
						}
					}
				}
				$.ajaxq('submit_channel', settings);
 			}
 		}),
 		/* 6 */
 		(function(module, exports, __webpack_require__) {
 			"use strict";
 			var pushthru = __webpack_require__(1);
 			var runtime = __webpack_require__(2);
 			var AppHandshakes = (function () {
 				function AppHandshakes() {
 					this.approved = false;
 					this.connected = false;
 					this.sessionid = null;
 				}
 				AppHandshakes.prototype.startAuth = function (app_key, callback) {
 					var _this = this;
 					$.ajaxq('initial_handshake', {
	 					url: runtime["default"].api_url+'api/script/'+app_key+'/0',
	 					dataType: 'json',
	 					success: function(response) {
	 						if (response) {
	 							_this.approved = true;
 								if (typeof callback === "function") callback();
		 					} else {
 								runtime["default"].log("App key did not pass!", "error");
			 				}
		 				},
		 				error: function(a,b,c) {
							runtime["default"].log("App key did not pass! Please check your internet connection", "error");
			 			}
	 				});
 				};
 				AppHandshakes.prototype.connect = function (callback, stashes) {
 					var _this = this;
 					if (_this.connected == false) {
 						var ws_protocol = 'wss://', port = runtime["default"].wss_port;
 						if ( ! window.location.protocol.match('https')) {
 							ws_protocol = 'ws://';
 							port = runtime["default"].ws_port;
 						}
 						pushthru["default"].socket = new ab.Session(ws_protocol + runtime["default"].ws_host + ':' + port,
 							function(sessionid, state, ws_version) {
 								_this.connected = true;
 								_this.sessionid = sessionid;
 								runtime["default"].log("App Connected!", null, 1);
 								if (typeof callback === "function") callback();
 							},
 							function(sessionid, state, ws_version) {
 								_this.connected = false;
 								_this.sessionid = null;
 								runtime["default"].log("App Disconnected!!", "error", 1);
 								runtime["default"].log("App Reconnecting...", null, 1);
 								setTimeout(function() {
 									_this.connect(function(){
 										stashes.exec(function(_stashes) {
 											/*_stashes.clear();*/
 										});
 									}, stashes);
 								}, 3000);
 							},
 							{'skipSubprotocolCheck': true}
 						);
 					}
 				};
 				AppHandshakes.prototype.disconnect = function () {
 					var _this = this;
 					if (_this.approved && _this.connected == true) {
 						pushthru["default"].socket.close();
 					}
 				};
 				return AppHandshakes;
 			}());
 			exports.__esModule = true;
 			exports["default"] = AppHandshakes;
 		}),
 		/* 6 */
 		(function(module, exports, __webpack_require__) {
 			"use strict";
 			var pushthru = __webpack_require__(1);
 			var runtime = __webpack_require__(2);
 			var Stash = (function () {
 				function Stash() {
 					this.list = {};
 					this.object = {};
 					this.args = {};
 					this.method = {};
 				}
 				Stash.prototype.exec = function (callback) {
 					var _this = this;
 					for (var name in _this.list) {
 						var method_name = _this.method[name];
 						_this.object[name][method_name].apply(_this.object[name], _this.args[name]);
 					}
 					if (typeof callback == 'function') {
 						callback(_this);
 					}
 					return _this;
 				};
 				Stash.prototype.add = function (name, object, params, method) {
					this.list[name] = name;
					this.object[name] = object;
					this.args[name] = params;
					this.method[name] = method;
 					return this;
 				};
 				Stash.prototype.all = function () {
 					return this.list;
 				};
 				Stash.prototype.find = function (name) {
 					return this.list[name];
 				};
 				Stash.prototype.remove = function (name) {
 					delete this.list[name];
 					return this;
 				};
 				Stash.prototype.clear = function () {
 					for (var name in this.list) {
 						delete this.list[name];
 						delete this.object[name];
 						delete this.args[name];
 						delete this.method[name];
 					}
 					return this;
 				};
 				return Stash;
 			}());
 			exports.__esModule = true;
 			exports["default"] = Stash;
 		})
 	])
});	