<?php

if (isset($_POST['channel'])) {
	$context = new ZMQContext();
	$subs = $context->getSocket(ZMQ::SOCKET_SUB);

	$subs->setSockOpt(ZMQ::SOCKOPT_SUBSCRIBE, $_POST['channel']);
	$subs->connect("tcp://127.0.0.1:5566", true);

	$listener = new ZMQPoll();
	$listener->add($subs, ZMQ::POLL_IN);

	set_time_limit(0);
	$read = $write = array();
	while (true) {
		$event = $listener->poll($read, $write, 1);
		if ($event > 0) {
			// echo $subs->recv(); exit;
			echo json_encode(array('data' => $subs->recv())); exit;
		}
	}
}