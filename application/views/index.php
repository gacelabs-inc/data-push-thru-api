<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<base href="<?php echo base_url();?>">
	<title></title>

	<link href="assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link href="assets/css/custom.css" rel="stylesheet">
	<script type="text/javascript" src="assets/public/js/jquery.min.js"></script>
	<script src="assets/public/js/bootstrap.min.js"></script>
	<script src="assets/public/js/fontawesome.js"></script>
</head>

<div class="hide">
	<div id="message-boxes">
		<ul>
			<li class="left clearfix" id="left">
				<span class="chat-img pull-left">
					<img src="assets/images/user.png" alt="User Avatar" class="img-circle">
				</span>
				<div class="chat-body clearfix">
					<p class="pull-left">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia.</p>
				</div>
				<div class="chat_time text-center" style="font-size: 8px;">09:40PM</div>
			</li>
			<li class="left clearfix admin_chat" id="right">
				<span class="chat-img pull-right">
					<img src="assets/images/user.png" alt="User Avatar" class="img-circle">
				</span>
				<div class="chat-body clearfix">
					<p class="pull-right">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia.</p>
				</div>
				<div class="chat_time text-center" style="font-size: 8px;">09:40PM</div>
			</li>
		</ul>
	</div>
	<div id="user-boxes">
		<ul>
			<li class="left clearfix">
				<span class="chat-img pull-left">
					<img src="assets/images/user.png" alt="User Avatar" class="img-circle">
				</span>
				<div class="chat-body clearfix">
					<div class="header_sec">
						<strong class="primary-font alias_name">Poi Garcia</strong> <strong class="pull-right date_entered">
						09:45AM</strong>
					</div>
					<div class="update_sec">
						<strong class="primary-font is_typing"></strong> <span class="badge pull-right msg_count"></span>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>

<body>
	<div class="container">
		<div class="col-md-10">
			<h3 class="text-muted">Enter your name to subcribe Chatroom</h3>
			<div class="row">
				<div class="input-group">
					<input type="text" value="" placeholder="Your name here" name="username" class="required form-control username">
					<span class="input-group-btn">
						<input type="submit" class="button btn btn-default" value="Subscribe" id="subscribe">
					</span>
				</div>
			</div>
		</div>
	</div>

	<div class="main_section">
		<div class="container">
			<div class="chat_container">
				<div class="col-sm-3 chat_sidebar">
					<div class="row">
						<!-- <div id="custom-search-input">
							<div class="input-group col-md-12">
								<input type="text" class="  search-query form-control" placeholder="Conversation" />
								<button class="btn btn-danger" type="button">
									<span class=" glyphicon glyphicon-search"></span>
								</button>
							</div>
						</div>
						<div class="dropdown all_conversation">
							<button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-weixin" aria-hidden="true"></i>
								All Conversations
								<span class="caret pull-right"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
									<li><a href="#"> All Conversation </a>  <ul class="sub_menu_ list-unstyled">
										<li><a href="#"> All Conversation </a> </li>
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li><a href="#">Separated link</a></li>
									</ul>
								</li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li><a href="#">Separated link</a></li>
							</ul>
						</div> -->
						<div class="member_list" style="font-size: x-small;">
							<ul class="list-unstyled"></ul>
						</div>
					</div>
				</div>
				<!--chat_sidebar-->

				<div class="col-sm-9 message_section">
					<div class="row">
						<!-- <div class="new_message_head">
							<div class="pull-left"><button><i class="fa fa-plus-square-o" aria-hidden="true"></i> New Message</button></div><div class="pull-right"><div class="dropdown">
								<button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cogs" aria-hidden="true"></i>  Setting
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
									<li><a href="#">Action</a></li>
									<li><a href="#">Profile</a></li>
									<li><a href="#">Logout</a></li>
								</ul>
							</div></div>
						</div> --><!--new_message_head-->

						<div class="chat_area">
							<ul class="list-unstyled"></ul>
						</div><!--chat_area-->
						<div class="message_write" id="">
							<textarea class="form-control" id="message" msg-sessionId="" placeholder="type a message" style="resize: vertical;"></textarea>
							<div class="clearfix"></div>
							<div class="chat_bottom">
								<!-- <a href="#" class="pull-left upload_btn"><i class="fa fa-cloud-upload" aria-hidden="true"></i>Add Files</a> -->
								<a href="#" class="pull-right btn btn-success" id="send">Send <i class="fa fa-send" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div> <!--message_section-->
			</div>
		</div>
	</div>
	<script type="text/javascript" id="push-thru-scripts" src="get/jsfile/<?php echo APP_KEY;?>"></script>
	<!-- <script type="text/javascript" id="push-thru-scripts" src="assets/private/js/PushThru.js"></script> -->
	<script type="text/javascript">
		var pushthru = new PushThru('<?php echo APP_KEY;?>');
		var sessionId = false;

		$(document).ready(function() {
			pushthru.subscribe('join_chat').listen('join', function(obj) {
				var uiUser = $('#user-boxes li').clone();
				uiUser.attr('id', obj.data.sessionId);
				$('#message').attr('msg-sessionId', obj.data.sessionId);
				uiUser.find('.alias_name').text(obj.data.user);
				var d = new Date();
				uiUser.find('.date_entered').text(timeNow());
				$('.member_list').find('ul').append(uiUser);
				// $(this).closest('div.container').hide();
			});
			pushthru.subscribe('chatroom').listen('sent', function(obj) {
				if (obj.data.sessionId != sessionId) {
					constructChatBox('left', obj.data.message);
					$('.update_sec strong.is_typing').html('');
					if (typeof obj.callback == 'function') obj.callback();
				}
			});
			pushthru.subscribe('is_typing').listen('typing', function(obj) {
				if (sessionId !== obj.data.sessionId) {
					$('#'+obj.data.sessionId).find('.update_sec strong.is_typing').html('<img src="assets/images/typing.gif" class="img-responsive">');
				}
			});
			pushthru.subscribe('logging_out').listen('logout', function(obj) {
				$('#'+obj.data.sessionId).remove();
			});

			$('#subscribe').on('click', function(e) {
				if ($.trim($('[name="username"]').val()) != '') {
					sessionId = pushthru.handshakes.sessionid; // Math.floor(Math.random() * 1000000);
					var username = $.trim($('.username').val());
					pushthru.trigger('join', 'join_chat', {
						'sessionId': sessionId,
						'user': username
					});
				}
			});
			$('input.username').on('keyup', function(e) {
				if ((e.which | e.keyCode) == 13 && $.trim($(this).val()) != '') {
					$('#subscribe').trigger('click');
				}
			});
			$('#message').on('keydown', function(e) {
				if (e.ctrlKey && e.keyCode === 13) {
					$('#send').trigger('click');
				}
			}).on('keypress', function() {
				if ($.trim($('#message').val()) != '' && $.trim($('#message').val()).length <= 3) {
					pushthru.trigger('typing', 'is_typing', {
						'sessionId': sessionId
					});
				}
			});
			$('#send').on('click', function(e) {
				if ($.trim($('#message').val()) != '') {
					var message = $.trim($('#message').val());
					pushthru.trigger('sent', 'chatroom', {
						'sessionId': sessionId,
						'message': message
					});
					constructChatBox('right', message);
					$('#message').val('');
					$('.update_sec strong.is_typing').html('');
				}
			});

			window.onbeforeunload = function(e) {
				(e && e.preventDefault) ? e.preventDefault() : e.returnValue;
				pushthru.trigger('logout', 'logging_out', {
					'sessionId': sessionId
				});
				return undefined;
			};
		});

		function timeNow() {
			var d = new Date(),
			h = (d.getHours()<10?'0':'') + d.getHours(),
			m = (d.getMinutes()<10?'0':'') + d.getMinutes();
			var ampm = h >= 12 ? 'PM' : 'AM';
			if (ampm == 'PM') {
				return (h-12) + ':' + m + ' ' + ampm;
			} else {
				return h + ':' + m + ' ' + ampm;
			}
		}

		function dayNow() {
			var d = new Date();
			var weekday = new Array(7);
			weekday[0] =  "SUN";
			weekday[1] = "MON";
			weekday[2] = "TUES";
			weekday[3] = "WED";
			weekday[4] = "THURS";
			weekday[5] = "FRI";
			weekday[6] = "SAT";
			return weekday[d.getDay()];
		}

		function constructChatBox(loc, msg) {
			var uiMsg = $('#message-boxes ul li#'+loc).clone();
			uiMsg.find('.chat-body p').html(msg);
			uiMsg.find('.chat_time').text(dayNow()+' '+timeNow());
			uiMsg.find('.chat-img img').attr('title', $('#user-boxes li#'+sessionId).find('.chat-body .alias_name').text());
			$('.chat_area').find('ul').append(uiMsg);
			$('.chat_area').scrollTop($('.chat_area')[0].scrollHeight);
		}
	</script>
</body>

</html>