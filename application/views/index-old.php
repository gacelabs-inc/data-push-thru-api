<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<base href="<?php echo base_url();?>">

	<style type="text/css">
		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>

	<script src="assets/public/js/jquery.min.js"></script>
	<!-- <script src="assets/public/js/ajaxq.js"></script> -->
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter!</h1>

	<div id="body">
		
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>

	<form id="sample" method="post" enctype="multipart/form-data">
		<select id="category" name="category">
			<option value="">-- Select --</option>
			<option value="kittensCategory">Kittens</option>
			<option value="puppiesCategory">Puppies</option>
		</select>
		<input type="text" name="title">
		<textarea id="article"></textarea>
		<button type="submit">Send</button>
	</form>
	<form id="generate_files" method="post" action="generate_files/15549E1EA97B4C2CD016D04DA276582D" enctype="multipart/form-data">
		<button type="submit">Generate My Files</button>
	</form>
	<button type="submit" id="remove">Remove Generated Files</button>
</div>

<!-- <script type="text/javascript" src="assets/js/test.js"></script> -->
<!-- <script type="text/javascript" src="assets/public/js/autobahn.js"></script> -->
<script type="text/javascript" id="push-thru-scripts" src="assets/private/js/PushThru.js"></script>
<!-- <script type="text/javascript" src="api/jsfile/15549E1EA97B4C2CD016D04DA276582D"></script> -->
<script>
	// PushThru('15549E1EA97B4C2CD016D04DA276582D');
	var pushthru = new PushThru('15549E1EA97B4C2CD016D04DA276582D');

	$(document).ready(function() {
		$('#sample').off('submit').on('submit', function(e) {
			e.preventDefault();
			pushthru.trigger($('#category').val(), 'connection', $('#article').val());
		});
		$('#generate_files').off('submit').on('submit', function(e) {
			e.preventDefault();
			$.ajaxq('generate_files', {
				url: $(this).attr('action'),
				dataType: 'json',
				success: function(data) {
					$('#message').remove();
					$('#js_file').remove();
					$('#php_file').remove();
					$('#container').append('<div id="message">'+data.message+'</div>')
					$('#container').append('<div id="js_file">'+data.js_file.toString()+'</div>')
					$('#container').append('<div id="php_file">'+data.php_file+'</div>')
				}
			});
		});
		$('#remove').off('click').on('click', function() {
			$('#message').remove();
			$('#js_file').remove();
			$('#php_file').remove();
		});

		$('#category').off('change').on('change', function() {
			if ($(this).val() != '') {
				pushthru.subscribe('connection').bind($(this).val(), {
					whiteListed: false,
					blackListed: false,
					callback: function(obj) {
						$('#body').append(obj.data+'<br>');
					}
				});
			}
		});
	});
</script>
</body>
</html>