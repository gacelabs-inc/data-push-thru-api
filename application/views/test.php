<!DOCTYPE html>
<html>
<head>
	<title>TEST</title>
	<base href="<?php echo $base_url;?>">
</head>
<body>
	<div id="message">
		
	</div>
	<input type="text" value="" id="name">
	<button id="subscribe">Subscribe!</button>
	<button id="send">Send!</button>
	<!-- <iframe src="subscriber.php"></iframe> -->
	<script type="text/javascript" src="assets/public/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/public/js/ajaxq.js"></script>
	<script type="text/javascript">
		var isRunning = false;
		function subscribe() {
			$.ajaxq('subscribe', {
				url : 'subscriber.php',
				type: 'post',
				dataType: 'json',
				data: {
					channel: $('#name').val(),
					event: 'join'
				},
				success: function(data) {
					console.log(data);
					console.log('event received!');
					subscribe();
				}
			});
		} 

		$(document).ready(function(e) {
			$('#send').click(function(e) {
				$.ajaxq('trigger', {
					url : 'sender.php',
					type: 'post',
					data: {
						channel: $('#name').val(),
						event: 'message'
					},
					success: function(data) {
						console.log(data);
					}
				});
			});

			$('#subscribe').click(function(e) {
				if (isRunning == false) {
					subscribe();
					isRunning = true;
				}
			});
		});
	</script>
</body>
</html>
