<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebApp extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function ajax()
	{
		echo json_encode(array('test'=>TRUE));
	}

	public function app()
	{
		$this->load->view('app', array('base_url' => base_url()));
	}

	public function test()
	{
		$this->load->view('test', array('base_url' => base_url()));
	}

	public function index()
	{
		$this->load->view('index', array('base_url' => base_url()));
	}

	public function jsfiles($app_key)
	{
		if ($company = $this->auth($app_key, TRUE)) {
			$this->load->view('generate/push_thru_jsfile', array('endpoint'=>base_url('app/transmit/'.$company->app_secret.'/0')));
		} else {
			echo "/*PLEASE SIGN UP WITH US @ ".base_url('register')."*/";
		}
	}

	public function auth($app_key='', $backend=0)
	{
		$auth = $company = FALSE;
		if ($app_key != '') {
			/*checking of app key here*/
			$companies = $this->db->query("SELECT * FROM companies WHERE app_key = '$app_key' AND allowed = 1");
			if ($companies->num_rows()) {
				$company = $companies->row();
				if ($company->expired == 0) {
					header('Access-Control-Allow-Origin: '.$company->origin);
					$is_origin_same = TRUE;
					$referrer = parse_url($this->agent->referrer());
					if ($backend != 2) {
						$is_origin_same = $company->domain === $referrer['host'];
					}
					$match = strtoupper(md5($company->origin.$company->id.$company->domain));
					// debug($match);
					if ($match === $app_key AND $is_origin_same) {
						$auth = TRUE;
					}
					/*and check also if this domain is a lifetime user*/
					if ($company->lifetime == 1 AND $is_origin_same) {
						$auth = TRUE;
					}
				}
			}
		}
		if ($backend==1) {
			return $company;
		} else if ($backend==2) {
			echo json_encode($company);
		} else {
			echo json_encode($auth);
		}
	}

	public function transmit($app_secret='', $backend=0) /*make this method multiple routing or changing uri for security purposes*/
	{
		$entrydata = $this->input->post();
		$app_key = isset($entrydata['app_key']) ? $entrydata['app_key'] : NULL;
		$response = $this->catch_response('successful');
		/*checking again of app key*/
		if ($company = $this->auth($app_key, TRUE)) {
			/*make sure app_secret is same in database, if not.. well PushThru js file was copied only*/
			if ($company->app_secret == $app_secret AND $company->expired == 0) {
				if (isset($entrydata['type'])) {
					$type = $entrydata['type'];
					unset($entrydata['type']); unset($entrydata['app_key']);
					$entrydata['company_id'] = $company->id;
					$check = $this->db->get_where('channels', $entrydata);
					if ($type == 'add_channel') {
						if ($check->num_rows() == 0) {
							$this->db->insert('channels', $entrydata);
							$response = $this->catch_response('channel_created');
						}
					} else { /*remove_channel*/
						if ($check->num_rows() > 0) {
							$this->db->delete('channels', $entrydata);
							$response = $this->catch_response('channel_removed');
						}
					}
				} else {
					$entrydata['when'] = date('Y-m-d H:i:s');
					// This is our new stuff
					$context = new ZMQContext();
					$socket = $context->getSocket(ZMQ::SOCKET_PUSH);
					$socket->connect("tcp://127.0.0.1:5555");
					$socket->send(json_encode($entrydata));
					$response = $this->catch_response('successful');
				}
			} else {
				/*set new app_secret for this company*/
				// $new_app_secret = md5($this->encryption->encrypt($company->domain.generate_random_string()));
				// $this->db->update('companies', array('app_secret' => $new_app_secret), array('id' => $company->id));
				$response = $this->catch_response('transmission_not_allowed');
			}
		} else {
			$response = $this->catch_response('no_pass');
		}

		if ($backend == 1) {
			return $response;
		} else {
			echo json_encode($response);
		}
	}
	
	private function catch_response($response='forbidden')
	{
		switch ($response) {
			case 'channel_created':
				http_response_code(200);
				return array('code' => 200, 'text' => 'Channel has been created.');
				break;
			case 'channel_removed':
				http_response_code(200);
				return array('code' => 200, 'text' => 'Channel has been removed.');
				break;
			case 'transmission_not_allowed':
				http_response_code(404);
				return array('code' => 404, 'text' => 'You are not allowed transmitting data.');
				break;
			case 'no_pass':
				http_response_code(403);
				return array('code' => 403, 'text' => 'Your APP key might be expired or did not pass, Please settle this @ '.base_url('support'));
				break;
			case 'successful':
				http_response_code(200);
				return array('code' => 200, 'text' => 'Successful transmission');
				break;
			
			default: /*forbidden*/
				http_response_code(403);
				return array('code' => 403, 'text' => 'Connection to app forbidden.');
				break;
		}
	}

	public function check_channels($app_key='', $backend=0)
	{
		$response = FALSE;
		if ($company = $this->auth($app_key, TRUE)) {
			$channel = $this->input->post('channel');
			$check = $this->db->get_where('channels', array('id' => $company->id, 'channel' => $channel));
			if ($check->num_rows()) {
				$response = TRUE;
			}
		}
		if ($backend == 1) {
			return $response;
		} else {
			echo json_encode($response);
		}
	}

	public function generate_files($app_key='')
	{
		if ($company = $this->auth($app_key, TRUE)) {
			$php_file = $this->load->view('generate/push_thru_class', array('base_url' => base_url()), TRUE);
			$js_file = '&#60;script type="text/javascript" id="push-thru-scripts" src="'.base_url('jsfile/'.$app_key).'"&#62;&#60;/script&#62;';
			$files = array(
				'php_file' => $php_file,
				'js_file' => $js_file,
				'generated' => TRUE,
				'message' => 'Successfully generated'
			);
		} else {
			$files = array(
				'php_file' => 'Unable to generate php file, APP key not registered',
				'js_file' => 'Unable to generate script tag, APP key not registered',
				'generated' => FALSE,
				'message' => 'PLEASE SIGN UP WITH US @ <a href="'.base_url('register').'">'.base_url('register').'</a>'
			);
		}
		echo json_encode($files);
	}
}
