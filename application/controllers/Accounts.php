<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('company');
	}

	public function register()
	{
		$post = $this->input->post();
		if ($post) {
			$this->company->check($post)->get_data();
		}
		debug($this->company);
	}

}