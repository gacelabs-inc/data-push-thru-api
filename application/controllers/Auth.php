<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$app_key = $this->input->post('app_key');
		$auth = 0;
		if ($app_key) {
			/*checking of app key here*/
			$companies = $this->db->query("SELECT * FROM companies WHERE app_key = '$app_key' AND  allowed = 1");
			if ($companies->num_rows()) {
				$company = $companies->row();
				header('Access-Control-Allow-Origin: '.$company->origin);

				/*$is_origin_same = 1;
				$referrer = parse_url($this->agent->referrer());
				if ($backend != 2) {
					$is_origin_same = $company->domain === $referrer['host'];
				}*/
				$match = strtoupper(md5($company->origin.$company->id.$company->domain.'4n6-g^L1nG-N@|||An'));

				if ($match === $app_key/* AND $is_origin_same*/) {
					$auth = 1;
				}
				/*and check also if this domain is a lifetime user*/
				if ($company->lifetime == 1/* AND $is_origin_same*/) {
					$auth = 1;
				}
			}
		}
		if ($auth == 0) {
			$this->subscription->disconnect("tcp://127.0.0.1:5566");
			$this->pusher->disconnect("tcp://127.0.0.1:5567");
		}
		echo $auth; exit();
	}

	public function subscribe()
	{
		$this->load->view('worker/subscriber');
	}

	public function trigger()
	{
		$this->load->view('worker/sender');
	}

	/*public function subscribe()
	{
		$channel = $this->input->post('channel');
		if ($channel) {
			$context = new ZMQContext();
			$subscription = $context->getSocket(ZMQ::SOCKET_SUB);
			$subscription->connect("tcp://127.0.0.1:5566");
			$subscription->setSockOpt(ZMQ::SOCKOPT_SUBSCRIBE, $channel);

			$listener = new ZMQPoll();
			$listener->add($subscription, ZMQ::POLL_IN);

			$read = $write = array();
			// this will listen to any event subscribed to this channel
			while (true) {
				$event = $listener->poll($read, $write, 1000000);
				if ($event > 0) {
					echo json_encode(array(
						'data' => $subscription->recv()
					));
					exit;
				}
			}
		}
	}*/

	/*public function trigger()
	{
		$channel = $this->input->post('channel');
		$event = $this->input->post('event');

		if ($channel AND $event) {
			debug(1);
			$channel = htmlspecialchars($channel);
			$event = htmlspecialchars($event);
		
			$context = new ZMQContext();
			$pusher = $context->getSocket(ZMQ::SOCKET_PUSH);
			$pusher->connect("tcp://127.0.0.1:5567");

			if ($event == 'join') {
				$pusher->send($event."\n", ZMQ::MODE_SNDMORE);
				$pusher->send($channel."\n");
			} elseif ($event == 'clear') {
				$pusher->send('clear'."\n");
			} else {
				$pusher->send($channel.': '.$event."\n");
				echo 'event sent!';
			}
		}
	}*/
}