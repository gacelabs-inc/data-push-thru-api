<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('ZMQWorker');
	}

	public function subscribe()
	{
		$this->zmqworker->subscribe();
		debug($this->zmqworker);
	}

	public function trigger()
	{
		$this->zmqworker->trigger();
		debug($this->zmqworker);
	}
}