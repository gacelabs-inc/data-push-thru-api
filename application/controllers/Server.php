<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Server extends MY_Controller {

	protected $context = NULL;
	protected $subscriber = NULL;
	protected $pusher = NULL;

	protected $address = FALSE;
	protected $message = NULL;

	public function __construct()
	{
		parent::__construct();
		$this->context = new ZMQContext();
		$this->subscriber = new ZMQSocket($context, ZMQ::SOCKET_SUB);
		$this->subscriber->connect("tcp://127.0.0.1:5555");
		$this->pusher = $this->context->getSocket(ZMQ::SOCKET_PUSH);
		$this->pusher->connect("tcp://127.0.0.1:5554");
	}
	
	public function subscribe()
	{
		$channel = $this->input->post('channel');
		$this->subscriber->setSockOpt(ZMQ::SOCKOPT_SUBSCRIBE, $channel);
		echo "subscribed to ".$channel;
		while ($this->address = $this->subscriber->recv()) {
		    $this->message = $this->subscriber->recv();
		    sleep(1);
		    $this->send();
		}
	}

	public function send()
	{
		$this->pusher->send(json_encode(array('address'=>$this->address, 'message'=>$this->message)));
	}

	
}