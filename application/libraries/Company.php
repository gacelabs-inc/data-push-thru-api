<?php

class Company/* extends ServerResponse*/ {

	public $id = 0;
	public $company = NULL;
	public $origin = NULL;
	public $domain = NULL;
	public $app_key = NULL;
	public $app_token = NULL;
	public $app_secret = NULL;
	public $is_trial = 1;
	public $lifetime = 0;

	public function __construct(array $config=array())
	{
		if (count($config)) {
			$this->initialize($config);
		}
	}

	private function merge_data($data=array())
	{
		foreach ($data as $key => $value) {
			switch ($key) {
				case 'app_key': case 'app_token': case 'app_secret':
					if (trim($value) == '' AND strlen(trim($value)) == 0) {
						$this->{$key} = $this->generate_keys($key);
					} else {
						$this->{$key} = $value;
					}
					break;
				default:
					$this->{$key} = $value;
					break;
			}
		}
		// debug($this);
		return $this;
	}

	private function generate_keys($type='app_key')
	{
		$ci =& get_instance();
		switch ($type) {
			case 'app_token':
				return $ci->encryption->encrypt($this->origin.$this->domain.generate_random_string());
				break;
			case 'app_secret':
				return md5($ci->encryption->encrypt($this->domain.generate_random_string()));
				break;
			default: /*app_key*/
				return strtoupper(md5($this->origin.$this->id.$this->domain));
				break;
		}
	}

	public function initialize(array $config=array())
	{
		if (count($config)) {
			/*check the id first if not 0 its an update else new*/
			$this->merge_data($config);
		} else {
			throw new Exception("Error Processing Request, Empty Parameters", 1);
		}
		return $this;
	}

	public function check(array $config=array())
	{
		if (count($config)) {
			$this->clear();
			$this->initialize($config);
		} else {
			throw new Exception("Error Processing Request, Empty Parameters", 1);
		}
		return $this;
	}

	public function get_data()
	{
		$ci =& get_instance();
		$data = $ci->db->get_where('companies', array('id' => $this->id));
		$this->clear();
		if ($data->num_rows()) {
			foreach ($data->row() as $key => $value) {
				$this->{$key} = $value;
			}
		}
		return $this;
	}

	public function clear()
	{
		$this->id = 0;
		$this->company = NULL;
		$this->origin = NULL;
		$this->domain = NULL;
		$this->app_key = NULL;
		$this->app_token = NULL;
		$this->app_secret = NULL;
		$this->is_trial = 1;
		$this->lifetime = 0;
		return $this;
	}

	public function add($data=array())
	{
		if (count((array)$data)) {
			$ci =& get_instance();
			$ci->db->insert('companies', $data);
			$this->id = $ci->db->insert_id();
		}
		return $this;
	}

	public function register()
	{
		if ($this->id == 0) {
			$this->date_registered = date('Y-m-d H:i:s');
			// debug($this);
			$data = $this;
			unset($data->id);
			$this->add($data);
			$this->get_data();
		}
		return $this;
	}
}