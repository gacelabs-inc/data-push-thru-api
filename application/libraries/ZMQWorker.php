<?php

class ZMQWorker {

	protected $context = NULL;
	protected $subscription = NULL;
	protected $listener = NULL;
	protected $pusher = NULL;
	protected $ci = NULL;

	public function __construct()
	{
		// debug($context);
		$this->ci =& get_instance(); 
		$this->context = new ZMQContext();
		$this->listener = new ZMQPoll();
		$this->subscription = $this->context->getSocket(ZMQ::SOCKET_SUB);
		$this->subscription->connect("tcp://127.0.0.1:5566");
		$this->pusher = $this->context->getSocket(ZMQ::SOCKET_PUSH);
		$this->pusher->connect("tcp://127.0.0.1:5567");
	}

	public function subscribe()
	{
		if ($this->ci->input->post('channel')) {
			$this->subscription->setSockOpt(ZMQ::SOCKOPT_SUBSCRIBE, $this->ci->input->post('channel'));

			$this->listener->add($this->subscription, ZMQ::POLL_IN);

			$read = $write = array();
			while (true) {
				$event = $this->listener->poll($read, $write, 1000000);
				if ($event > 0) {
					echo json_encode(array(
						'data' => $this->subscription->recv()
					)); exit;
				}
			}
		}
	}

	public function trigger()
	{
		if ($this->ci->input->post()) {
			$channel = htmlspecialchars($this->ci->input->post('channel'));
			$event = htmlspecialchars($this->ci->input->post('event'));

			// $pusher->send($event, ZMQ::MODE_SNDMORE);
			// $pusher->send($channel);
			$this->pusher->send(json_encode($this->ci->input->post()));
			/*if ($event == 'join') {
			} elseif ($event == 'clear') {
				$pusher->send('clear'."\n");
			} else {
				$pusher->send($channel.': '.$event."\n");
			}*/
			// echo 'event sent!';
		}
	}

}