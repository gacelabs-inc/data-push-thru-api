<?php
namespace WebApp;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use voku\db\DB;

class Pusher implements WampServerInterface {
	/**
	 * A lookup of all the events companies have subscribed to
	 */
	protected $events = array();
	protected $connections = array();
	protected $db = NULL;

	public function __construct()
	{
		$this->db = DB::getInstance('localhost', 'root', '', 'data_push_thru');
	}

	public function get_DB() {
		return $this->db;
	}

	public function onSubscribe(ConnectionInterface $conn, $event) {
		$event_id = $event->getId();
		echo "\ncreate event ".$event_id." for connection ".$conn->resourceId." @ ".$conn->remoteAddress;
		$this->events[$event_id] = $event;
		$this->connections[$event_id] = $conn;
	}

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
	public function onDataEntry($entry) {
		$data = json_decode($entry, true);
		/*If the lookup event dataect isn't set there is no one to publish to*/
		if (!array_key_exists($data['event'], $this->events)) return;

		$conn = $this->connections[$data['event']];
		echo "\n\nexecute event ".$data['event']." for connection ".$conn->resourceId." @ ".$conn->remoteAddress;
		echo "\npayload length -> ".strlen(json_encode($data))."\n";

		$event = $this->events[$data['event']];
		/*re-send the data to all the events subscribed to that connections*/
		$exclude = $data['exclude'] ? $event->getIterator() : array(); 
		$eligible = $data['eligible'] ? $event->getIterator() : array();
		unset($data['eligible']);
		unset($data['exclude']);
		$event->broadcast($data, $exclude, $eligible);
	}

	public function onUnSubscribe(ConnectionInterface $conn, $event) {
		$event_id = $event->getId();
		echo "\nunsubscribing connection ".$conn->resourceId." @ ".$conn->remoteAddress." for event: ".$event_id;
		unset($this->events[$event_id]);
		unset($this->connections[$event_id]);
		// $event->remove($conn);
	}

	public function onOpen(ConnectionInterface $conn) {
		echo "\nopening connection ".$conn->resourceId." @ ".$conn->remoteAddress;
		if (count($this->events)) {
			echo "\navailable events for ".$conn->resourceId." @ ".$conn->remoteAddress;
			foreach ($this->events as $key => $value) {
				echo "\n".$key;
			}
		}
		// echo "\nopening connection ".$conn->WAMP->sessionId." @ ".$conn->remoteAddress;
	}

	public function onClose(ConnectionInterface $conn) {
		echo "\nclosing connection ".$conn->resourceId." @ ".$conn->remoteAddress."...";
		echo "\n\nListening and waiting requests..";
	}

	public function onCall(ConnectionInterface $conn, $id, $event, array $params) {
		/*In this application if companies send data it's because the user hacked around in console*/
		$conn->callError($id, $event, 'You are not allowed to make calls')->close();
	}

	public function onPublish(ConnectionInterface $conn, $event, $method, array $exclude, array $eligible) {
		/*In this application if companies send data it's because the user hacked around in console*/
		$event->broadcast($event, $exclude, $eligible);
		/*$conn->close();*/
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		echo "\nerror encountered from connection ".$conn->resourceId." @ ".$conn->remoteAddress;
		echo "\nerror message -> ".$e->getMessage();
		/*$conn->close();*/
	}
}