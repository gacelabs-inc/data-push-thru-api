<?php
namespace WebApp;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use voku\db\DB;

class Pusher implements WampServerInterface {
	/**
	 * A lookup of all the channels companies have subscribed to
	 */
	protected $channels = array();
	protected $connections = array();
	protected $blacklist = array();
	protected $whitelist = array();
	protected $db = NULL;

	public function __construct()
	{
		$this->db = DB::getInstance(
			'localhost', 
			((isset($_SERVER['USERDOMAIN']) AND $_SERVER['USERDOMAIN'] == 'BONG-LAPTOP') ? 'root' : 'pushthru'), 
			((isset($_SERVER['USERDOMAIN']) AND $_SERVER['USERDOMAIN'] == 'BONG-LAPTOP') ? '' : 'D4t4Pu5h7hrU!'), 
			'data_push_thru_db'
		);
	}

	public function get_DB() {
		return $this->db;
	}

	public function onSubscribe(ConnectionInterface $conn, $channel) {
		$channel_id = $channel->getId();
		echo "\n".date('F j, Y | g:i:s a')." | ".$conn->resourceId." @ ".$conn->remoteAddress.": Channel ".$channel_id." created";
		$this->channels[$channel_id] = $channel;
		$this->connections[$channel_id] = $conn;
	}

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
	public function onDataEntry($entry) {
		$event = json_decode($entry, true);
		unset($event['app_key']);
		/*If the lookup channel dataect isn't set there is no one to publish to*/
		if (!array_key_exists($event['channel'], $this->channels)) return;

		$conn = $this->connections[$event['channel']];
		echo "\n".date('F j, Y | g:i:s a')." | ".$conn->resourceId." @ ".$conn->remoteAddress.": Execute event ".$event['event']." to channel ".$event['channel'];
		echo "\n".date('F j, Y | g:i:s a')." | ".$conn->resourceId." @ ".$conn->remoteAddress.": Payload character length ".strlen(json_encode($event['data']));
		$channel = $this->channels[$event['channel']];

		/*re-send the data to all the channels subscribed to that connections*/
		$blacklist = $whitelist = array();
		if (isset($event['blacklist'])) {
			if ($event['blacklist']) {
				$this->blacklist[$conn->WAMP->sessionId] = $conn->WAMP->sessionId;
			} else {
				unset($this->blacklist[$conn->WAMP->sessionId]);
			}
		}
		if (isset($event['whitelist'])) {
			if ($event['whitelist']) {
				$this->whitelist[$conn->WAMP->sessionId] = $conn->WAMP->sessionId;
			} else {
				unset($this->whitelist[$conn->WAMP->sessionId]);
			}
		}
		unset($event['whitelist']); unset($event['blacklist']);

		$this->onPublish($conn, $channel, $event, $this->blacklist, $this->whitelist);
	}

	public function onUnSubscribe(ConnectionInterface $conn, $channel) {
		$channel_id = $channel->getId();
		echo "\n".date('F j, Y | g:i:s a')." | ".$conn->resourceId." @ ".$conn->remoteAddress.": Unsubscribing connection for channel: ".$channel_id;
		unset($this->channels[$channel_id]);
		unset($this->connections[$channel_id]);
		$channel->remove($conn);
	}

	public function onOpen(ConnectionInterface $conn) {
		echo "\n\n".date('F j, Y | g:i:s a')." | ".$conn->resourceId." @ ".$conn->remoteAddress.": Opening connection";
		sleep(0.5);
	}

	public function onClose(ConnectionInterface $conn) {
		echo "\n".date('F j, Y | g:i:s a')." | ".$conn->resourceId." @ ".$conn->remoteAddress.": Closing connection";
		echo "\n\n".date('F j, Y | g:i:s a').": Listening and waiting requests";
		$conn->close();
	}

	public function onCall(ConnectionInterface $conn, $id, $channel, array $params) {
		/*In this application if companies send data it's because the user hacked around in console*/
		$conn->callError($id, $channel, 'You are not allowed to make calls')->close();
	}

	public function onPublish(ConnectionInterface $conn, $channel, $event, array $exclude, array $eligible) {
		/*sending event on the subscribe channel*/
		$channel->broadcast($event, $exclude, $eligible);
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		echo "\n".date('F j, Y | g:i:s a')." | ".$conn->resourceId." @ ".$conn->remoteAddress.": Error encountered, ".$e->getMessage();
		$conn->close();
	}
}