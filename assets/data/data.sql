-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for data_push_thru
DROP DATABASE IF EXISTS `data_push_thru`;
CREATE DATABASE IF NOT EXISTS `data_push_thru` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `data_push_thru`;

-- Dumping structure for table data_push_thru.channels
DROP TABLE IF EXISTS `channels`;
CREATE TABLE IF NOT EXISTS `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- Dumping data for table data_push_thru.channels: 0 rows
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;

-- Dumping structure for table data_push_thru.companies
DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` tinytext,
  `origin` text,
  `domain` text,
  `app_key` varchar(32) DEFAULT NULL,
  `app_secret` varchar(32) DEFAULT NULL,
  `app_token` longtext,
  `allowed` tinyint(1) NOT NULL DEFAULT '1',
  `is_trial` tinyint(1) NOT NULL DEFAULT '1',
  `lifetime` tinyint(1) NOT NULL DEFAULT '0',
  `date_registered` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table data_push_thru.companies: 1 rows
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `company`, `origin`, `domain`, `app_key`, `app_secret`, `app_token`, `allowed`, `is_trial`, `lifetime`, `date_registered`) VALUES
	(1, 'Data Push Thru', 'http://local.datapushthru.com.ph/', 'local.datapushthru.com.ph', '3CA9DE3495131AC4FC31A14C8A222BA1', '543e8064642a290b0723f365837ad119', 'bd2c1fa83e16bc83eaff89d59f500844b4d061654adae6709af34a0f3ea46cb697e614c9c202ca0730320e493ab42c60a19883d04d7beb3ef748b21b7c304a81HJ4qQMY8mx8I/Vt+eajhXygm59WXGGKgnObHYpeKU6g=', 1, 1, 0, '2018-09-16 13:40:47');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;

-- Dumping structure for table data_push_thru.trial_data
DROP TABLE IF EXISTS `trial_data`;
CREATE TABLE IF NOT EXISTS `trial_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- Dumping data for table data_push_thru.trial_data: 0 rows
/*!40000 ALTER TABLE `trial_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `trial_data` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
