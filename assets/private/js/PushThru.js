/*! * PushThru JavaScript Library v1.0.0 * http://www.datapushthru.com/ * * Copyright 2018, DataPushThru.com */
(function webpackUniversalModuleDefinition(root, factory) {
    if (typeof exports === "object" && typeof module === "object") {
        module.exports = factory();
    } else if (typeof define === "function" && define.amd) {
        define([], factory);
    } else if (typeof exports === "object") {
        exports["PushThru"] = factory();
    } else {
        root["PushThru"] = factory();
    }
})(this, function() {
    return (function(modules) {
        var installedModules = {};

        function __webpack_require__(moduleId) {
            if (installedModules[moduleId]) {
                return installedModules[moduleId].exports;
            }
            var module = installedModules[moduleId] = {
                exports: {},
                id: moduleId,
                loaded: false
            };
            modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
            module.loaded = true;
            return module.exports;
        }
        __webpack_require__.m = modules;
        __webpack_require__.c = installedModules;
        __webpack_require__.p = "";
        return __webpack_require__(0);
    })
    /************************************************************************/ 
    ([ /* 0 */ (function(module, exports, __webpack_require__) {
        "use strict";
        var pushthru = __webpack_require__(1);
        module.exports = pushthru["default"];
    }), /* 1 */ (function(module, exports, __webpack_require__) {
        "use strict";
        var runtime = __webpack_require__(2);
        var plant = __webpack_require__(3);
        var PushThru = (function() {
            function PushThru(app_key) {
                var _this = this;
                checkAppKey(app_key);
                PushThru.key = app_key;
                this.events = plant["default"].createEvents();
                this.stashes = plant["default"].createStashes();
                this.handshakes = plant["default"].createHandshakes();
                if (PushThru.is_loaded) {
                    var i = setInterval(function() {
                        if (document.readyState === "complete") {
                            clearInterval(i);
                            _this.handshakes.startAuth(app_key, function() {
                                _this.handshakes.connect(function() {
                                    _this.stashes.exec(function(stashes) { /*stashes.clear();*/ });
                                }, _this.stashes);
                            });
                        }
                    }, 500);
                }
            }
            PushThru.run = function() {
                PushThru.is_loaded = true;
            };
            PushThru.prototype.log = function(message, type, force) {
                runtime["default"].log(message, type, force);
            }; /*manage channels*/
            PushThru.prototype.allChannels = function() {
                return this.events.channels.all();
            };
            PushThru.prototype.channel = function(name) {
                return this.events.channels.find(name);
            };
            PushThru.prototype.getSessionId = function() {
                return this.events.channels.getSession();
            };
            PushThru.prototype.subscribe = function(name) {
                if (this.handshakes.connected) { /*this.events.channels.setSession(this.handshakes.sessionid);*/
                    this.events.channels.add(name);
                } else {
                    this.stashes.add(this, [name], 'subscribe');
                }
                return this;
            };
            PushThru.prototype.unsubscribe = function(name) {
                return this.events.channels.remove(name);
            }; /*manage events*/
            PushThru.prototype.allEvents = function() {
                return this.events.all();
            };
            PushThru.prototype.event = function(name) {
                return this.events.find(name);
            };
            PushThru.prototype.bind = function(name, channel, callback) {
                if (this.handshakes.connected) { /*this.events.channels.setSession(this.handshakes.sessionid);*/
                    this.events.add(name, channel, callback);
                } else {
                    this.stashes.add(this, [name, channel, callback], 'bind');
                }
                return this;
            };
            PushThru.prototype.listen = function(name, callback) {
                if (this.handshakes.connected) {
                    this.events.bind(name, callback);
                } else {
                    this.stashes.add(this, [name, callback], 'listen');
                }
                return this;
            };
            PushThru.prototype.unbind = function(name) {
                return this.events.remove(name);
            };
            PushThru.prototype.trigger = function(event, channel, data) {
                if (this.handshakes.connected) {
                    this.events.send(event, channel, data);
                } else {
                    this.stashes.add(this, [event, channel, data], 'trigger');
                }
                return this;
            };
            PushThru.is_loaded = false;
            PushThru.version = '1.0.0';
            PushThru.key = null;
            PushThru.Runtime = runtime["default"];
            return PushThru;
        }());
        exports.__esModule = true;
        exports["default"] = PushThru;

        function checkAppKey(key) {
            if (key === null || key === undefined) {
                throw "You must pass your app key when you instantiate PushThru.";
            }
        }
        runtime["default"].setup(PushThru);
    }), /* 2 */ (function(module, exports) {
        "use strict";
        var Runtime = {
            ws_host: 'local.datapushthru.com.ph',
            ws_port: 8080,
            wss_port: 443,
            api_url: 'http://local.datapushthru.com.ph/',
            endpoint: 'http://local.datapushthru.com.ph/app/transmit/543e8064642a290b0723f365837ad119/0',
            setup: function(PushThruClass) {
                var _this = this;
                window.PushThru = PushThruClass;
                var initializeOnDocumentBody = function() {
                    _this.onDocumentBody(PushThruClass.run);
                };
                checkScript((typeof window.jQuery == 'function' || typeof window.$ == 'function'), _this.api_url + 'assets/public/js/jquery.min.js', true).then(() => {
                    checkScript((typeof window.JSON == 'object'), _this.api_url + 'assets/public/js/json2.js', false);
                }).then(() => {
                    checkScript(typeof window.ab == 'object', _this.api_url + 'assets/public/js/autobahn.js', false);
                }).then(() => {
                    checkScript((typeof window.jQuery.ajaxq == 'function' || typeof window.$.ajaxqq == 'function'), _this.api_url + 'assets/public/js/ajaxq.js', false);
                }).then(() => {
                    return initializeOnDocumentBody();
                });
            },
            getDocument: function() {
                return document;
            },
            getProtocol: function() {
                return this.getDocument().location.protocol;
            },
            onDocumentBody: function(callback) {
                var _this = this;
                if (document.body) {
                    callback();
                } else {
                    setTimeout(function() {
                        _this.onDocumentBody(callback);
                    }, 0);
                }
            },
            log: function(data, type, force) {
                switch (type) {
                    case 'warn':
                        console.warn("%c\u2620 " + data, "font-weight:bold;line-height:18px;");
                        break;
                    case 'error':
                        console.error("%c\u26d4 " + data, "font-weight:bold;line-height:18px;");
                        break;
                    default:
                        console.log("%c\u26a1 " + data, "font-weight:bold;line-height:18px;");
                        break;
                }
            }
        };
        exports.__esModule = true;
        exports["default"] = Runtime;

        function loadScript(url, is_in_head) {
            return new Promise(function(resolve, reject) {
                var script = document.createElement("script");
                script.type = 'text/javascript';
                script.onload = resolve;
                script.onerror = reject;
                script.src = url;
                if (is_in_head) {
                    document.getElementsByTagName("head")[0].appendChild(script);
                } else {
                    $(script).insertBefore($('#push-thru-scripts'));
                }
            });
        }

        function checkScript(isSet, url, is_in_head) {
            if (isSet) {
                return Promise.resolve();
            } else {
                return loadScript(url, is_in_head);
            }
        }
    }), /* 3 */ (function(module, exports, __webpack_require__) {
        "use strict";
        var events = __webpack_require__(4);
        var channels = __webpack_require__(5);
        var handshakes = __webpack_require__(6);
        var stashes = __webpack_require__(7);
        var Plant = {
            createChannels: function() {
                return new channels["default"]();
            },
            createEvents: function() {
                return new events["default"]();
            },
            createStashes: function() {
                return new stashes["default"]();
            },
            createHandshakes: function() {
                return new handshakes["default"]();
            }
        };
        exports.__esModule = true;
        exports["default"] = Plant;
    }), /* 4 */ (function(module, exports, __webpack_require__) {
        "use strict";
        var pushthru = __webpack_require__(1);
        var runtime = __webpack_require__(2);
        var plant = __webpack_require__(3);
        var Events = (function() {
            function Events() {
                this.list = {};
                this.channels = plant["default"].createChannels();
            }
            Events.prototype.add = function(event, channel, callback) {
                if (this.channels.find(event + ':' + channel) == undefined) {
                    this.channels.setSession(this.channels.getSession());
                    this.channels.add(event + ':' + channel);
                }
                if (typeof callback != 'function') {
                    runtime["default"].log("Event Callback is not a function on binded event " + event + " for channel " + channel, "error");
                } else {
                    var eventFunction = function(eventname, obj) {
                        callback(obj, channel);
                    };
                    pushthru["default"].socket.subscribe(channel, eventFunction);
                    this.list[event] = pushthru["default"].socket;
                }
                return this;
            };
            Events.prototype.bind = function(event, callback) { /*this.channels.remove(this.channels.saved);*/
                this.add(event, this.channels.saved, callback);
                this.channels.saved = null;
                return this;
            };
            Events.prototype.all = function() {
                return this.list;
            };
            Events.prototype.find = function(event) {
                return this.list[event];
            };
            Events.prototype.remove = function(event, callback) {
                var socket = this.list[event];
                delete this.list[event];
                socket.unsubscribe(event, callback);
                return this;
            };
            Events.prototype.send = function(event, channel, push_data) {
                if (typeof this.channels.find(event + ':' + channel) != 'undefined' && typeof this.find(event) != 'undefined') {
                    var object = {
                        'channel': channel,
                        'event': event,
                        'app_key': pushthru["default"].key,
                        'blacklist': [],
                        'whitelist': [],
                        'broadcast': true,
                        'data': null
                    }
                    if (typeof push_data.blacklist == 'object' && push_data.blacklist.length) {
                        object.blacklist.push(push_data.blacklist);
                    }
                    if (typeof push_data.whitelist == 'object' && push_data.whitelist.length) {
                        object.whitelist.push(push_data.whitelist);
                    }
                    if (typeof push_data.broadcast == 'boolean') {
                        object.broadcast = push_data.broadcast;
                    }
                    object.data = push_data;
                    var settings = {
                        url: runtime["default"].endpoint,
                        type: 'post',
                        data: object,
                        success: function(response) {
                            if (typeof push_data.ajax_options != 'undefined') {
                                $.ajaxq('owners_send', push_data.ajax_options);
                            }
                            runtime["default"].log("Data transmitted!");
                        },
                        error: function(xhr, code, status) {
                            if (xhr.status == 404) {
                                runtime["default"].log("You are not allowed transmitting data!", "error");
                            } else {
                                runtime["default"].log("Your APP key might be expired or did not pass, Please settle this @ " + runtime["default"].api_url + "support", "error");
                            }
                        }
                    }
                    $.ajaxq('default_send', settings);
                } else {}
                return this;
            };
            return Events;
        }());
        exports.__esModule = true;
        exports["default"] = Events;
    }), /* 5 */ (function(module, exports, __webpack_require__) {
        "use strict";
        var pushthru = __webpack_require__(1);
        var runtime = __webpack_require__(2);
        var plant = __webpack_require__(3);
        var Channels = (function() {
            function Channels() {
                this.list = {};
                this.saved = null;
                this.sessionid = null;
            }
            Channels.prototype.add = function(name) {
                if (this.find(name) == undefined) {
                    this.list[name] = plant["default"].createEvents();
                    this.saved = name;
                    if (name.indexOf(':') >= 0) {
                        submitChannel(name, 'add_channel', this.sessionid);
                    }
                }
                return this;
            };
            Channels.prototype.setSession = function(sessionid) {
                this.sessionid = sessionid;
                return this;
            };
            Channels.prototype.getSession = function() {
                return this.sessionid;
            };
            Channels.prototype.all = function() {
                return this.list;
            };
            Channels.prototype.find = function(name) {
                return this.list[name];
            };
            Channels.prototype.remove = function(name) {
                submitChannel(name, 'remove_channel', this.sessionid);
                delete this.list[name];
                return this;
            };
            return Channels;
        }());
        exports.__esModule = true;
        exports["default"] = Channels;

        function submitChannel(name, type, sessionid) {
            var settings = {
                url: runtime["default"].endpoint,
                type: 'post',
                /*data: {'type': type, 'app_key': pushthru["default"].key, 'channel': name, 'sessionid': sessionid},*/ 
                data: {
                    'type': type,
                    'app_key': pushthru["default"].key,
                    'channel': name
                },
                crossDomain: true,
                success: function(response) { /*if (type == 'add_channel') { runtime["default"].log(name+" channel subscribed!"); } else { runtime["default"].log(name+" channel unsubscribed!"); }*/ },
                error: function(xhr, code, status) {
                    if (xhr.status == 404) {
                        runtime["default"].log("You are not allowed transmitting data!", "error");
                    } else {
                        runtime["default"].log("Your APP key might be expired or did not pass, Please settle this @ " + runtime["default"].api_url + "support", "error");
                    }
                }
            }
            $.ajaxq('submit_channel', settings);
        }
    }), /* 6 */ (function(module, exports, __webpack_require__) {
        "use strict";
        var pushthru = __webpack_require__(1);
        var runtime = __webpack_require__(2);
        var AppHandshakes = (function() {
            function AppHandshakes() {
                this.approved = false;
                this.connected = false;
                this.sessionid = null;
            }
            AppHandshakes.prototype.startAuth = function(app_key, callback) {
                var _this = this;
                $.ajaxq('initial_handshake', {
                    url: runtime["default"].api_url + 'api/script/' + app_key + '/0',
                    dataType: 'json',
                    success: function(response) {
                        if (response) {
                            _this.approved = true;
                            if (typeof callback === "function") callback();
                        } else {
                            runtime["default"].log("App key did not pass!", "error");
                        }
                    },
                    error: function(a, b, c) {
                        runtime["default"].log("App key did not pass! Please check your internet connection", "error");
                    }
                });
            };
            AppHandshakes.prototype.connect = function(callback, stashes) {
                var _this = this;
                if (_this.connected == false) {
                    var ws_protocol = 'wss://',
                        port = runtime["default"].wss_port;
                    if (!window.location.protocol.match('https')) {
                        ws_protocol = 'ws://';
                        port = runtime["default"].ws_port;
                    }
                    pushthru["default"].socket = new ab.Session(ws_protocol + runtime["default"].ws_host + ':' + port, function(sessionid, state, ws_version) {
                        _this.connected = true;
                        _this.sessionid = sessionid;
                        runtime["default"].log("App Connected!", null, 1);
                        if (typeof callback === "function") callback();
                    }, function(state) {
                        _this.connected = false;
                        _this.sessionid = null;
                        runtime["default"].log("App disconnected!!", "error", 1);
                        switch (state) {
                            case ab.CONNECTION_CLOSED:
                                runtime["default"].log("App connection closed... ", null, 1);
                                break;
                            case ab.CONNECTION_LOST:
                                runtime["default"].log("App connection lost... reconnecting after 9 seconds", null, 1);
                                setTimeout(function() {
                                    _this.connect(function() {
                                        stashes.exec(function(_stashes) { /*_stashes.clear();*/ });
                                    }, stashes);
                                }, 9000);
                                break;
                            case ab.CONNECTION_UNREACHABLE:
                                runtime["default"].log("App connection unreachable...", null, 1);
                                runtime["default"].log("App reconnecting...", null, 1);
                                setTimeout(function() {
                                    _this.connect(function() {
                                        stashes.exec(function(_stashes) { /*_stashes.clear();*/ });
                                    }, stashes);
                                }, 3000);
                                break;
                            case ab.CONNECTION_UNSUPPORTED:
                                runtime["default"].log("App connection unsupported...", null, 1);
                                break;
                        }
                    }, {
                        'skipSubprotocolCheck': true
                    });
                }
            };
            AppHandshakes.prototype.disconnect = function() {
                var _this = this;
                if (_this.approved && _this.connected == true) {
                    pushthru["default"].socket.close();
                }
            };
            return AppHandshakes;
        }());
        exports.__esModule = true;
        exports["default"] = AppHandshakes;
    }), /* 6 */ (function(module, exports, __webpack_require__) {
        "use strict";
        var pushthru = __webpack_require__(1);
        var runtime = __webpack_require__(2);
        var Stash = (function() {
            function Stash() {
                this.trunk = [];
            }
            Stash.prototype.exec = function(callback) {
                var _this = this;
                for (var x in this.trunk) {
                    var stash = this.trunk[x];
                    var object = stash.object,
                        args = stash.args,
                        method = stash.method;
                    object[method].apply(object, args);
                }
                if (typeof callback == 'function') {
                    callback(_this);
                }
                return _this;
            };
            Stash.prototype.add = function(object, params, method) {
                var main = {
                    object: object,
                    args: params,
                    method: method
                }
                this.trunk.push(main);
                return this;
            };
            Stash.prototype.clear = function() {
                this.trunk.splice(0, this.trunk.length);
                return this;
            };
            return Stash;
        }());
        exports.__esModule = true;
        exports["default"] = Stash;
    })])
});